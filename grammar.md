- noun 
- verb 
- adjective
- adverb 
- Preposition 

src;https://www.englishclub.com/grammar/prepositions-list.htm

# Noun
Adalah kata yang menamai sesuatu, Seperti orang, tempat, benda atau sebuah ide. 
Didalam kalimat, nouns berperan sebagai subject, direct object, indirect object, 
subject complement, object complement, appositive, atau adjective 

Src: https://www.grammarly.com/blog/nouns/ 

# Verb 

# Adjective 

# Adverb 

Adverb adalah kata yang digunakan untuk menjelaskan / modifies dari verb. 
Adverb biasaanya berakhiran dengan -ly, akan tetapi beberapa seperti ( such as fast)
terlihat sama seperti adjective. 

Contoh:
Tom LongBoat did not run **badly**
Tom is **very** tall 
The race finished **too** quickly 
**Fortunately**, Lucy recorder Tom's win. 

Adverbs sering digunakan untuk menjelaskan verb. Dengan kata lain itu digunakan untuk 
menjelaskan gimana pekerjaan itu dilakukan. 

contoh: 
Philip sings **loudly** in the shower 
My cat waits **impatiently** for his food
I will **seriously** consider your suggestion

Adverb di kalimat diatas menjawab pertanyaan seperti *apa tindakan?* gimana philip bernyanyi?
Loudly. Gimana kucingku menunggu? Impatienly. Gimana aku memikirkan saran mu? Seriously. 

Adverb bisa menjawab gimana tindakan tersebut dilakukan. Dan juga digunakan untuk menjelaskan 
kapan ( We arrived early ) dan dimana ( Turn here)

Akan tetapi ada verb yang tidak cocok menggunakan adverb, seperti. Linking verbs 
( feel, smell, sound, seem, and appear, typically need adjectives, not adverbs.)

Sebagai contoh:
i feel **badly** about what happened. 

karena 'feel' adalah verb, sepertinnya memangil adverb dari pada adjective. Tapi 
'feel' tidak hanya verb; ini juga adalah linking verb. 
> Adverb akan menjelaskan gimana kamu melakukan tindakan/action - sedangkan adjective akan menjelaskan apa yang kau rasakan 

dikalimat diatas, ' i feel badly' berarti kau merasakan tidak enak akan sesuatu. 

### Adverbs and adjectives 
adverb juga bisa menjelaskan adjective dan adverb lainnya. Sering, tujuannya untuk 
adverb adalah menambahkan derajat intensitas dari adjective. 

Contoh: 
The woman is **quite** pretty 
This book is **more** interesting than last one 
The weather report is **almost always** right

dalam kata diatas, adverb 'always' menjelaskan kata 'right'

Contoh: 
My cat is **incredibly** happy to have his dinner

#### Adverb dan adjective 
You can use an adverb to describe another adverb. In fact, if you wanted to, you could use several.

Contoh: Phillip sings rather enormously too loudly.
The problem is that it often produces weak and clunky sentences like the one above, so be careful not to overdo it.

SRC: https://www.grammarly.com/blog/adverb/

# Preposition 

digunakan untuk menunjuk jalan, tempat dan waktu

preposition digunakan untuk menunjuk object. 
contoh:

at night
above you


What Is a Preposition?
“Vampires! Zombies! Werewolves!”

“Where?!”

“Behind you!”

Thank goodness for prepositions. Imagine not knowing where the danger lay….

Prepositions tell us where or when something is in relation to something else. When monsters are approaching, it’s good to have these special words to tell us where those monsters are. Are they behind us or in front of us? Will they be arriving in three seconds or at midnight?

Prepositions often tell us where one noun is in relation to another (e.g., The coffee is on the table beside you). But they can also indicate more abstract ideas, such as purpose or contrast (e.g., We went for a walk despite the rain).

Types of Prepositions
Prepositions indicate direction, time, location, and spatial relationships, as well as other abstract types of relationships.

Direction: Look to the left and you’ll see our destination.

Time: We’ve been working since this morning.

Location: We saw a movie at the theater.

Space: The dog hid under the table.

Preposition Examples
Unfortunately, there’s no reliable formula for determining which preposition to use with a particular combination of words. The best way to learn which prepositions go with which words is to read as much high-quality writing as you can and pay attention to which combinations sound right. Here are a few examples of the most common prepositions used in sentences.


I should rewrite the introduction of my essay.

Sam left his jacket in the car.

Did you send that letter to your mother?

We’re cooking for ten guests tonight.

Dan ate lunch with his boss.
You can also use tools like Google Ngrams to see which prepositions most commonly occur with particular words—but remember, this tool can’t explain the difference in meaning between different prepositional phrases like “pay for” (to purchase) and “pay off” (to bribe). For that, you may want to refer to a list of prepositions that includes the meanings of common combinations.

Ending a Sentence With a Preposition
The old claim that it’s wrong to end a sentence with a preposition has been utterly debunked. It’s not true and it never was true. Writers who always insist that a preposition can’t end a sentence often end up with stilted and unnatural sentences:


There’s no one else to hide behind . (Grammatically correct and natural)

There’s no one else behind whom to hide. (Grammatically correct, but unnatural)

Where did you come from ? (Grammatically correct and natural)

From where did you come? (Grammatically correct, but unnatural)
That said, it is sometimes more elegant to move a preposition to an earlier spot in a sentence, especially in very serious and formal writing. But if you do move the preposition, remember to delete it from the end.


This is something we must meditate on .

This is something on which we must meditate.

This is something on which we must meditate on .
Unnecessary Prepositions
One of the most common preposition mistakes is adding an unnecessary at to the end of a question.


Where is your brother at ?
Although this is common in some English dialects, it’s considered an error in writing. You can fix the problem by simply deleting the at.


Where is your brother?
On the bright side, if you’re not sure which preposition to use, sometimes you can just get rid of it altogether. In fact, you should always get rid of unnecessary prepositional phrases. Too many prepositions can be a sign of flabby writing. Look at how many prepositions appear in the sentence below:


For many people, the reality of an entry into a new area of employment is cause for a host of anxieties.
Getting rid of the prepositions forces you to tighten up the sentence. The result is shorter, more direct, and easier to understand:


Changing careers makes many people anxious.
Here’s another example:


Alex hit the baseball up over the fence.
Get rid of the up. You don’t need it:


Alex hit the baseball over the fence.

src: https://www.grammarly.com/blog/prepositions/

#note

Nouns name people, places, things, or ideas. 
Pronouns take the place of nouns. 
Verbs name actions or states of being. 
Adjectives modify nouns. 
Adverbs modify verbs, adjectives, and other adverbs.
Prepositions show the relationship between a noun or a pronoun (object of the preposition) and the rest of the sentence. 
Conjunctions connect words, phrases, or clauses. 
Interjections are words that show emotion and are not grammatically connected to the rest of the sentence.

src https://www.english-grammar-revolution.com/grammar-quizzes.html
